from flask import Flask, jsonify, request
import db_w


app = Flask(__name__)


@app.route('/todo/', methods=['GET'])
def get_tasks():
    return jsonify(db_w.read_data())


@app.route('/add/', methods=['POST'])
def new_task():
    db_w.add_task([request.json['title'], request.json['description']])
    return '200'


@app.route('/del/', methods=['POST'])
def del_task():
    db_w.del_task_by_name(request.json['title'])
    return '200'


@app.route('/complete/', methods=['POST'])
def complete_task():
    db_w.complete_task_by_name(request.json['title'])
    return '200'


if __name__ == '__main__':
    app.config["JSON_SORT_KEYS"] = False
    app.run()
