import sqlite3
from datetime import datetime


def create_connection():
    global c, db
    db = sqlite3.connect('main.db')
    c = db.cursor()


def read_data():
    '''
    вывод всех тасков
    '''
    create_connection()
    tasks = []
    for r in c.execute('select * from Tasks'):
        task = {
            'title': r[1],
            'description': r[2],
            'create date': r[3],
            'finish date': r[4]
        }
        tasks.append(task)
    return dict(tasks=tasks)


def add_task(task):
    '''
    Создание таска в таблице

    формат таска  - список [заголовок, описание]
    Дата создания - текущая дата
    Так же в таблицу добавлен ключ-автоинкремент для сохранения уникальности данных
    '''
    create_connection()
    ddate = datetime.now().strftime("%d.%m.%Y %H:%M")
    c.execute('insert into Tasks(Title, Desc, dcreate) values("{}","{}","{}")'.format(
        task[0], task[1], ddate))
    db.commit()


def del_task_by_name(task_title):
    '''
    Удаление таска по заголовку
    '''
    create_connection()
    c.execute('delete from Tasks where Title="{}"'.format(task_title))
    db.commit()


def complete_task_by_name(task_title):
    '''
    Отметить задачу как выполненую

    если поле dfinish заполнено таск считается выполненым
    '''
    create_connection()
    ddate = datetime.now().strftime("%d.%m.%Y %H:%M")
    c.execute('update Tasks set dfinish="{}" where Title="{}"'.format(
        ddate, task_title))
    db.commit()


def main():
    # процедура для отладки, не используется в rest приложении
    task = ['task8', 'description1']
    add_task(task)
    del_task_by_name(task[0])
    complete_task_by_name(task[0])
    print(read_data())


if __name__ == '__main__':
    main()
